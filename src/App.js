import track1 from "./assets/track1.jpg";
import track2 from "./assets/track2.jpg";
import track3 from "./assets/track3.jpg";
import trackPlayer1 from "./assets/track1.mp3";
import trackPlayer2 from "./assets/track2.mp3";
import trackPlayer3 from "./assets/track3.mp3";
import AudioPlayer from "./components/AudioPlayer";

import { useEffect, useState } from "react";

function App() {
	const tracks = [
		{
			playTrack: track1, // image
			displayName: "Yellow", // name
			artist: "Coldplay", // artist
			musicTrack: trackPlayer1, // song
			id: 1,
		},
		{
			playTrack: track2, // image
			displayName: "Fix you", // name
			artist: "Coldplay - X & Y", // artist
			musicTrack: trackPlayer2, // song
			id: 2,
		},
		{
			playTrack: track3, // image
			displayName: "Life in Technicolor", // name
			artist: "Coldplay - Viva La Vida", // artist
			musicTrack: trackPlayer3, // song
			id: 3,
		},
	];

	const [cms, setCMS] = useState([]);

	useEffect(() => {
		const fetchData = async () => {
			const response = await fetch(
				"https://heroku-sample-cms.herokuapp.com/api/songs"
			);
			const resp = await response.json();
			console.log(resp);
			const { data } = resp;
			setCMS(data);
		};
		fetchData();
	}, []);

	return (
		<div>
			<AudioPlayer tracks={tracks} cms={cms} />
		</div>
	);
}

export default App;
