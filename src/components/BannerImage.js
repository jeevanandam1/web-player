const BannerImage = ({ playTrack }) => {
	return (
		// Banner
		<div className="md:w-3/5 md:h-2/5">
			<img
				className="rounded md:block md:h-[285px] w-full"
				src={playTrack}
				alt="Album Pic"
			/>
		</div>
	);
};

export default BannerImage;
