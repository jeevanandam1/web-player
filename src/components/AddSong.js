import { useState } from "react";
import AddForm from "./AddForm";

export default function AddSong({ addEvent, handleClick }) {
	// states
	const [songName, setSongName] = useState("");
	const [songAuthor, setSongAuthor] = useState("");

	const handleName = (e) => {
		setSongName(e.target.value);
	};

	const handleAuthor = (e) => {
		setSongAuthor(e.target.value);
	};

	const resetForm = () => {
		setSongName("");
		setSongAuthor("");
	};

	// handleSubmit
	const handleSubmit = (e) => {
		e.preventDefault();
		const data = Math.floor(Math.random() * 100);
		const id = Math.floor(Math.random() * 10000);
		const detail = {
			displayName: songName,
			artist: songAuthor,
			playTrack: `https://randomuser.me/api/portraits/men/${data}.jpg`,
			id: id,
		};
		console.log(detail);
		addEvent(detail);
		setSongName("");
		setSongAuthor("");
	};

	return (
		// form
		<div>
			<AddForm
				handleAuthor={handleAuthor}
				handleClick={handleClick}
				handleSubmit={handleSubmit}
				handleName={handleName}
				resetForm={resetForm}
				songName={songName}
				songAuthor={songAuthor}
			/>
		</div>
	);
}
