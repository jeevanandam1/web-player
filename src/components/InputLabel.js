export default function InputLabel({ name, handleInput, song }) {
	return (
		<div>
			<label className="block mb-1 font-semibold text-gray-700">{name}</label>
			<input
				type="text"
				className="w-full px-4 py-2 rounded-md outline-none bg-slate-100"
				onChange={handleInput}
				value={song}
				required
			/>
		</div>
	);
}
