export default function DropDown({ listArr }) {
	return (
		<div className="absolute z-10 right-5">
			<ul>
				<li className="flex flex-col items-center justify-center text-white ease-in-out bg-green-400 rounded-lg">
					{listArr.map((list) => (
						<a key={list} className="px-4 py-2 font-semibold" href="/">
							{list}
						</a>
					))}
				</li>
			</ul>
		</div>
	);
}
