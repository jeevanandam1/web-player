export default function Button({ handleFunc, children, width }) {
	return (
		<button
			onClick={handleFunc}
			className={
				width
					? `w-auto px-2 py-2 mt-4 text-sm font-semibold tracking-wider text-white bg-green-400 rounded-md`
					: `w-full px-2 py-1 mt-4 text-sm font-semibold tracking-wider text-white bg-green-400 rounded-md`
			}
		>
			{children}
		</button>
	);
}
