export default function ButtonIcon({ handleIcon, direction, children }) {
	return (
		<button
			type="button"
			className={
				children
					? `pause py-3.5 px-5 text-white bg-green-500 rounded-full shadow-lg cursor-pointer`
					: `text-gray-700 cursor-pointer prev`
			}
			aria-label="Previous"
			onClick={handleIcon}
		>
			<i className={`fa-solid ${direction}`}></i>
			{children}
		</button>
	);
}
