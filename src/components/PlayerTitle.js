const PlayerTitle = ({ displayName, artist }) => {
	return (
		<div className="flex justify-between">
			<div>
				<h3 className="text-2xl font-bold text-gray-700">{displayName}</h3>
				<p className="mt-1 text-sm font-bold text-gray-400">{artist}</p>
			</div>
		</div>
	);
};

export default PlayerTitle;
