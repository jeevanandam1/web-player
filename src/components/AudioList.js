import { useState } from "react";
import AddSong from "./AddSong";
import Song from "./Song";
import TrackList from "./TrackList";

const AudioList = ({ tracks }) => {
	// states
	const [show, setShow] = useState(false);
	let [songTrack, setSongTrack] = useState(tracks);

	// handleClick
	const handleClick = () => {
		setShow(!show);
	};

	// addTrack
	const addEvent = (current) => {
		setSongTrack((prev) => {
			return [...prev, current];
		});
		setShow(false);
	};

	// handleDelete
	const handleDelete = (id) => {
		setSongTrack((prevEvents) => {
			return prevEvents.filter((song) => {
				return id !== song.id;
			});
		});
	};

	return (
		<div>
			<TrackList songTrack={songTrack} handleDelete={handleDelete}>
				<p className="text-sm font-light text-gray-400 lg:text-md">
					Disc#1 Lorem ipsum dolor, sit amet consectetur adipisicing elit.
					Possimus culpa modi numquam.
				</p>
			</TrackList>
			{show && <AddSong addEvent={addEvent} handleClick={handleClick} />}
			{!show && <Song handleClick={handleClick}>Add New</Song>}
		</div>
	);
};

export default AudioList;
