import Navbar from "./Navbar";
import Recommended from "./Recommended";

export default function TopAlbum() {
	const list = [
		{
			tile: "Rerum illum consequuntur explicabo",
			image:
				"https://images.unsplash.com/photo-1660804880033-e63ead176bec?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw3fHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=600&q=60",
			artist: "Avicii",
			id: 1,
		},
		{
			tile: "Rerum illum consequuntur explicabo",
			image:
				"https://images.unsplash.com/photo-1660756010411-be7aed1b88cb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwxOHx8fGVufDB8fHx8&auto=format&fit=crop&w=600&q=60",
			artist: "Avicii",
			id: 2,
		},
		{
			tile: "Rerum illum consequuntur explicabo",
			image:
				"https://images.unsplash.com/photo-1660494510476-a8e549decdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4MHx8fGVufDB8fHx8&auto=format&fit=crop&w=600&q=60",
			artist: "Avicii",
			id: 3,
		},
	];

	return (
		// Navbar, Recommended
		<div>
			<Navbar />
			<Recommended list={list} />
		</div>
	);
}
