import Button from "./Button";

export default function TrackList({ songTrack, handleDelete, children }) {
	// TrackList
	return (
		<div className="border-t border-zinc-300">
			<div className="px-6 mt-4 lg:mt-6">{children}</div>
			{songTrack.map((track) => (
				<div key={track.id}>
					<ul className="flex items-center justify-between p-6 ">
						<li className="flex cursor-pointer">
							<img
								className="w-10 h-10 rounded-lg"
								src={track.playTrack}
								alt="song"
							/>
							<div className="ml-3 overflow-hidden">
								<p className="text-sm font-semibold text-slate-900">
									{track.displayName}
								</p>
								<p className="text-sm font-semibold text-slate-500">
									{track.artist}
								</p>
							</div>
						</li>
						<div>
							<Button handleFunc={() => handleDelete(track.id)}>Delete</Button>
						</div>
					</ul>
				</div>
			))}
		</div>
	);
}
