export default function Progress({
	// trackProgress,
	duration,
	songProgress,
	onScrub,
	onScrubEnd,
	trackStyling,
}) {
	// Progress
	return (
		<div>
			<input
				type="range"
				// value={trackProgress}
				value={songProgress}
				step="1"
				min="0"
				max={duration ? duration : `${duration}`}
				className="my-8 progress"
				onChange={(e) => onScrub(e.target.value)}
				onMouseUp={onScrubEnd}
				onKeyUp={onScrubEnd}
				style={{ background: trackStyling }}
			/>
		</div>
	);
}
