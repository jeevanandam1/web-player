import { useState } from "react";
import DropDown from "./DropDown";

export default function Navbar() {
	const [show, setShow] = useState(false);

	const handleClick = () => {
		setShow(!show);
	};

	const listArr = ["View Profile", "Version", "Log Out"];

	return (
		<div className="relative">
			<div className="flex items-center justify-between cursor-pointer">
				<p className="px-4 py-6 font-bold text-normal lg:text-2xl">
					Recommended for you
				</p>
				<div className="flex items-center justify-between gap-4 p-1 mx-4 bg-green-400 rounded-full">
					<div
						onClick={handleClick}
						className="md:hidden w-7 h-7 lg:w-10 lg:h-10 "
					>
						<img
							className="border-0 rounded-full md:border-4"
							src={"https://randomuser.me/api/portraits/men/17.jpg"}
							alt="user"
						/>
					</div>
					<div className="hidden md:block w-7 h-7 lg:w-10 lg:h-10 ">
						<img
							className="border-0 rounded-full md:border-4"
							src={"https://randomuser.me/api/portraits/men/17.jpg"}
							alt="user"
						/>
					</div>
					<p className="hidden font-bold text-white md:block">Shaun</p>
					<i className="hidden text-white md:block fa-solid fa-bell"></i>
					<i
						className={
							show
								? "hidden mr-3 text-white md:block fa-solid fa-chevron-up"
								: "hidden mr-3 text-white md:block fa-solid fa-chevron-down"
						}
						onClick={handleClick}
					></i>
				</div>
			</div>
			{show && <DropDown listArr={listArr} />}
		</div>
	);
}
