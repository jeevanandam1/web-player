import React, { useState, useEffect, useRef, useCallback } from "react";
import "../components/style.css";
import AudioList from "./AudioList";
import BannerImage from "./BannerImage";
import PlayerTrack from "./PlayerTrack";
import TopAlbum from "./TopAlbum";

const AudioPlayer = ({ cms, tracks }) => {
	console.log(tracks, "Tracks");
	console.log(cms, "Songs");

	// state
	// const [trackIndex, setTrackIndex] = useState(0);
	const [songIndex, setSongIndex] = useState(0);
	// const [trackProgress, setTrackProgress] = useState(0);
	const [songProgress, setSongProgress] = useState(0);
	const [isPlaying, setIsPlaying] = useState(false);

	// destructure
	// const { playTrack, displayName, artist, musicTrack } = tracks[trackIndex];

	const playTrack = cms[songIndex]?.attributes.playTrack;
	const displayName = cms[songIndex]?.attributes.displayName;
	const artist = cms[songIndex]?.attributes.artist;
	const musicTrack = cms[songIndex]?.attributes.musicTrack;

	// refs
	const audioRef = useRef(new Audio(musicTrack));
	const intervalRef = useRef();
	const isReady = useRef(false);

	// destructure
	const { duration } = audioRef.current;

	console.log(duration, "Duration");

	// const currentPercentage = duration
	// 	? `${(trackProgress / duration) * 100}%`
	// 	: "0%";
	// const trackStyling = `
	// 	-webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(${currentPercentage}, #374151), color-stop(${currentPercentage}, #4ADE80))
	//   `;

	const currentPercentage = duration
		? `${(songProgress / duration) * 100}%`
		: "0%";
	const trackStyling = `
	  -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(${currentPercentage}, #374151), color-stop(${currentPercentage}, #4ADE80))
	`;

	console.log(currentPercentage, "CurrentPercentage");

	// next
	const toNextTrack = useCallback(() => {
		// if (trackIndex < tracks.length - 1) {
		// 	setTrackIndex(trackIndex + 1);
		// } else {
		// 	setTrackIndex(0);
		// }
		if (songIndex < cms.length - 1) {
			setSongIndex(songIndex + 1);
		} else {
			setSongIndex(0);
		}
	}, [songIndex, cms.length]);

	const startTimer = useCallback(() => {
		// clear any timers already running
		clearInterval(intervalRef.current);

		intervalRef.current = setInterval(() => {
			if (audioRef.current.ended) {
				toNextTrack();
			} else {
				// setTrackProgress(audioRef.current.currentTime);
				setSongProgress(audioRef.current.currentTime);
			}
		}, [1000]);
	}, [toNextTrack]);

	const onScrub = (value) => {
		// clear any timers already running
		clearInterval(intervalRef.current);
		audioRef.current.currentTime = value;
		// setTrackProgress(audioRef.current.currentTime);
		setSongProgress(audioRef.current.currentTime);
	};

	const onScrubEnd = () => {
		// if not already playing, start
		if (!isPlaying) {
			setIsPlaying(true);
		}
		startTimer();
	};

	// prev
	const toPrevTrack = () => {
		// if (trackIndex - 1 < 0) {
		// 	setTrackIndex(tracks.length - 1);
		// } else {
		// 	setTrackIndex(trackIndex - 1);
		// }
		if (songIndex - 1 < 0) {
			setSongIndex(cms.length - 1);
		} else {
			setSongIndex(songIndex - 1);
		}
	};

	useEffect(() => {
		if (isPlaying) {
			audioRef.current.play();
			startTimer();
		} else {
			audioRef.current.pause();
		}
	});

	// handles cleanup and setup when changing tracks
	useEffect(() => {
		audioRef.current.pause();
		audioRef.current = new Audio(musicTrack);
		// setTrackProgress(audioRef.current.currentTime);
		setSongProgress(audioRef.current.currentTime);
		if (isReady.current) {
			audioRef.current.play();
			setIsPlaying(true);
			startTimer();
		} else {
			// set the isReady ref as true for the next pass
			isReady.current = true;
		}
	}, [songIndex, musicTrack, startTimer]);

	useEffect(() => {
		// pause and clean up on unmount
		return () => {
			audioRef.current.pause();
			clearInterval(intervalRef.current);
		};
	}, []);

	return (
		// Banner, PlayerTrack, AudioList
		<div className="w-full bg-green-400">
			<div className="flex items-center justify-center min-h-screen">
				<div className="w-11/12 mx-auto my-8 bg-white rounded-lg shadow-lg 2xl:w-8/12">
					<TopAlbum />
					<h1 className="p-1 mx-4 font-bold text-normal lg:text-2xl">
						Now Playing
					</h1>
					<div className="block p-1 mx-4 my-4 border rounded-lg border-zinc-300 md:flex">
						<BannerImage playTrack={playTrack} />
						<PlayerTrack
							displayName={displayName}
							artist={artist}
							isPlaying={isPlaying}
							toNextTrack={toNextTrack}
							toPrevTrack={toPrevTrack}
							setIsPlaying={setIsPlaying}
							// trackProgress={trackProgress}
							songProgress={songProgress}
							duration={duration}
							onScrubEnd={onScrubEnd}
							trackStyling={trackStyling}
							onScrub={onScrub}
						/>
					</div>
					<AudioList tracks={tracks} isPlaying={isPlaying} />
				</div>
			</div>
		</div>
	);
};

export default AudioPlayer;
