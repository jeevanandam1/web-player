import AudioControls from "../components/AudioControls";
import PlayerTitle from "./PlayerTitle";
import Progress from "./Progress";
import SongList from "./SongList";

const PlayerTrack = ({
	displayName,
	artist,
	isPlaying,
	toNextTrack,
	toPrevTrack,
	setIsPlaying,
	// trackProgress,
	songProgress,
	duration,
	onScrub,
	onScrubEnd,
	trackStyling,
}) => {
	return (
		// PlayerTitle, AudioControls, SongList, Progress
		<div className="w-full p-4 lg:py-4 lg:px-8">
			<PlayerTitle displayName={displayName} artist={artist} />
			<AudioControls
				isPlaying={isPlaying}
				onPrevClick={toPrevTrack}
				onNextClick={toNextTrack}
				onPlayPauseClick={setIsPlaying}
			/>
			<SongList>
				<div className="mt-4">
					<div>
						<i className="mr-2 text-green-400 fa-solid fa-headphones"></i>
						<span className="text-gray-400">Travel mix</span>
					</div>
					<div>
						<i className="mr-2 text-green-400 fa-solid fa-music"></i>
						<span className="mr-1 text-gray-400">3 Songs, about 15min</span>
					</div>
				</div>
			</SongList>
			<Progress
				// trackProgress={trackProgress}
				songProgress={songProgress}
				duration={duration}
				onScrub={onScrub}
				onScrubEnd={onScrubEnd}
				trackStyling={trackStyling}
			/>
		</div>
	);
};

export default PlayerTrack;
