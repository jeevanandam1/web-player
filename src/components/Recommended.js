export default function Recommended({ list }) {
	return (
		<div className="flex flex-wrap items-center justify-center gap-2 m-4 rounded md:justify-start">
			{list.map((val) => (
				<div
					key={val.id}
					className="relative max-w-xs border rounded-lg border-zinc-300"
				>
					<img
						className="w-full rounded-t-lg"
						src={val.image}
						alt="Sunset in the mountains"
					/>
					<div className="px-6 py-4">
						<div className="cursor-pointer">
							<div className="absolute bg-white rounded-full left-7 top-[12.5rem] w-7 h-7 "></div>
							<i className="absolute text-4xl text-green-500 top-48 fa-solid fa-circle-play"></i>
						</div>
						<p className="mt-2 mb-2 text-xl font-bold">{val.tile}</p>
						<p className="text-base text-gray-700">{val.artist}</p>
					</div>
				</div>
			))}
		</div>
	);
}
