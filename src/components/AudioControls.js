import ButtonIcon from "./ButtonIcon";

const AudioControls = ({
	isPlaying,
	onPlayPauseClick,
	onPrevClick,
	onNextClick,
}) => {
	console.log(isPlaying);
	// ButtonIcon
	return (
		<div className="flex items-center justify-between mt-8">
			<ButtonIcon
				handleIcon={onPrevClick}
				direction={"fa-backward-step"}
			></ButtonIcon>
			<ButtonIcon handleIcon={() => onPlayPauseClick(!isPlaying)}>
				<i className={isPlaying ? `fas fa-pause` : `fas fa-play`}></i>
			</ButtonIcon>
			<ButtonIcon
				handleIcon={onNextClick}
				direction={"fa-forward-step"}
			></ButtonIcon>
		</div>
	);
};

export default AudioControls;
