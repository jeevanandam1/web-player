import Button from "./Button";

export default function Song({ handleClick, children }) {
	return (
		<div className="p-4 text-center">
			<Button width={true} handleFunc={handleClick}>
				{children}
			</Button>
		</div>
	);
}
