import Button from "./Button";
import InputLabel from "./InputLabel";

export default function AddForm({
	handleAuthor,
	handleClick,
	handleName,
	handleSubmit,
	resetForm,
	songAuthor,
	songName,
}) {
	// InputLabel, Button
	return (
		<div>
			<div className="flex items-center justify-center w-full my-4">
				<form onSubmit={handleSubmit}>
					<div className="w-screen max-w-[18rem] lg:max-w-sm px-10 py-8 bg-white border rounded-xl">
						<div className="space-y-4">
							<h1 className="text-2xl font-semibold text-center text-gray-700">
								Add Song
							</h1>
							<InputLabel
								name={"Song Name"}
								handleInput={handleName}
								song={songName}
							/>
							<InputLabel
								name={"Artist"}
								handleInput={handleAuthor}
								song={songAuthor}
							/>
						</div>
						<div className="flex items-center justify-between gap-1">
							<Button>Add</Button>
							<Button handleFunc={resetForm}>Reset</Button>
							<Button handleFunc={handleClick}>Close</Button>
						</div>
					</div>
				</form>
			</div>
		</div>
	);
}
